import { Category } from './category.model';
import { Author } from './author.model';

export class Book {
    id: number;
    name: string;
    author: Author;
    categories: Category[];
    isbn: string;
}