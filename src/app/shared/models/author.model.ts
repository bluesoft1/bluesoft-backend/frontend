export class Author {
    id: number;
    name: string;
    lastName: string;
    birthday: Date;
}