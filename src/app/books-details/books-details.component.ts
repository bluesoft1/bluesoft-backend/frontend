import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from '../core/services/book.service';
import { Book } from '../shared/models/book.model';
import { Author } from '../shared/models/author.model';

@Component({
  selector: 'app-books-details',
  templateUrl: './books-details.component.html',
  styleUrls: ['./books-details.component.scss']
})

export class BooksDetailsComponent implements OnInit {

  book: Book = { id: 0, name: '', author: null, categories: null, isbn: '' };
  isLoadingResults = true;

  constructor(private route: ActivatedRoute, private bookService: BookService, private router: Router) { }

  ngOnInit(): void {
    this.getBookDetails(this.route.snapshot.params.id);
  }

  getBookDetails(id: number) {
    this.bookService.getBookById(id)
      .subscribe((data: any) => {
        this.book = data;
        console.log(this.book);
        this.isLoadingResults = false;
      });
  }
  
  deleteBook(id: number) {
    this.isLoadingResults = true;
    this.bookService.deleteBook(id)
      .subscribe(res => {
          this.isLoadingResults = false;
          this.router.navigate(['/books']);
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }
}
