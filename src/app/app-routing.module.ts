import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from "./books/books.component";
import { BooksAddComponent } from "./books-add/books-add.component";
import { BooksDetailsComponent } from "./books-details/books-details.component";
import { BooksEditComponent } from "./books-edit/books-edit.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: '/books',
    pathMatch: 'full'
  },
  {
    path: 'books',
    component: BooksComponent,
    data: { title: 'Lista de Libros' }
  },
  {
    path: 'books-add/:id',
    component: BooksAddComponent,
    data: { title: 'Agregar libro' }
  },
  {
    path: 'books-details/:id',
    component: BooksDetailsComponent,
    data: { title: 'Detalles del libro' }
  },
  {
    path: 'books-edit/:id',
    component: BooksEditComponent,
    data: { title: 'Editar libro' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
