import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Book } from '../../shared/models/book.model';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = 'https://localhost:5001/api/Book/';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(`${apiUrl}GetBooks/`)
      .pipe(
        tap(book => console.log('fetched Book')),
        catchError(this.handleError('getBook', []))
      );
  }

  getBookById(id: number): Observable<Book> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<Book>(url).pipe(
      tap(_ => console.log(`fetched book id=${id}`)),
      catchError(this.handleError<Book>(`getBookById id=${id}`))
    );
  }

  addBook(book: Book): Observable<Book> {
    return this.http.post<Book>(apiUrl, book, httpOptions).pipe(
      tap((c: Book) => console.log(`added Book w/ id=${c.id}`)),
      catchError(this.handleError<Book>('addBook'))
    );
  }

  updateBook(id: number, book: Book): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.put(url, book, httpOptions).pipe(
      tap(_ => console.log(`updated Book id=${id}`)),
      catchError(this.handleError<any>('updateBook'))
    );
  }

  deleteBook(id: number): Observable<Book> {
    const url = `${apiUrl}/${id}`;
    return this.http.delete<Book>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted Book id=${id}`)),
      catchError(this.handleError<Book>('deleteBook'))
    );
  }
}
